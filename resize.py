import io
from PIL import Image
import os, fnmatch
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


def get_scale(width, height):
    if width*(8/7) > height:
        if width > 1000:
            return 700.0/width
    elif height > 800:
        return 800.0/height
    return 1


def resize_image(image, scale):
    #get image size
    width, height = image.size
    #calculate new size
    new_width = int(width*scale)
    new_height = int(height*scale)
    #resize image
    image.thumbnail((new_width, new_height), Image.ANTIALIAS)
    return image


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


paths = find('*.png', '/tmp/latex/')

def lambda_handler():
    #get image from s3 bucket
    in_mem_file = io.BytesIO()
    for path in paths:
        #convert image to PIL image
        image = Image.open(path)
        original_format = image.format.lower()
        #get the scale
        scale = get_scale(image.width, image.height)
        #resize image
        image = resize_image(image, scale)
        #save image and send to bucket
        image.save(path, format=original_format, quality=30)
        in_mem_file.seek(0)


lambda_handler()
