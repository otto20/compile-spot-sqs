import os, fnmatch
from pdfrw import PdfReader, PdfWriter


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


paths = find('*.pdf', '/tmp/latex/')


for path in paths:
    trailer = PdfReader(path)
    trailer.Info = {}
    PdfWriter(path, trailer=trailer).write()