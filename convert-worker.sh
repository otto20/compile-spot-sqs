#!/bin/bash

INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
REGION=%REGION%
S3BUCKET=%S3BUCKET%
S3BUCKETDESTINATION=%S3BUCKETDESTINATION%
SQSQUEUE=%SQSQUEUE%
AUTOSCALINGGROUP=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=aws:autoscaling:groupName" | jq -r '.Tags[0].Value')
COUNTER=0
while sleep 5; do

  JSON=$(aws sqs --output=json get-queue-attributes \
    --queue-url $SQSQUEUE \
    --attribute-names ApproximateNumberOfMessages)
  MESSAGES=$(echo "$JSON" | jq -r '.Attributes.ApproximateNumberOfMessages')

  if [ $MESSAGES -eq 0 ]; then

    continue

  fi

  JSON=$(aws sqs --output=json receive-message --queue-url $SQSQUEUE)
  RECEIPT=$(echo "$JSON" | jq -r '.Messages[] | .ReceiptHandle')
  BODY=$(echo "$JSON" | jq -r '.Messages[] | .Body')

  if [ -z "$RECEIPT" ]; then

    logger "$0: Empty receipt. Something went wrong."
    continue

  fi

  logger "$0: Found $MESSAGES messages in $SQSQUEUE. Details: JSON=$JSON, RECEIPT=$RECEIPT, BODY=$BODY"

  INPUT=$(echo "$BODY" | jq -r '.Records[0] | .s3.object.key')
  FNAME=$(echo $INPUT | rev | cut -f2 -d"." | rev | tr '[:upper:]' '[:lower:]')
  FEXT=$(echo $INPUT | rev | cut -f1 -d"." | rev | tr '[:upper:]' '[:lower:]')

  if [ "$FEXT" = "zip" ]; then

    logger "$0: Found work to convert. Details: INPUT=$INPUT, FNAME=$FNAME, FEXT=$FEXT"

    logger "$0: Running: aws autoscaling set-instance-protection --instance-ids $INSTANCE_ID --auto-scaling-group-name $AUTOSCALINGGROUP --protected-from-scale-in"

    export PATH=/var/task/texlive/2017/bin/x86_64-linux/:$PATH

    export PERL5LIB=/var/task/texlive/2017/tlpkg/TeXLive/

    aws autoscaling set-instance-protection --instance-ids $INSTANCE_ID --auto-scaling-group-name $AUTOSCALINGGROUP --protected-from-scale-in

    aws s3 cp s3://$S3BUCKET/$INPUT /tmp/latex/

    unzip /tmp/latex/compile.zip -d /tmp/latex/

    cd /tmp/latex/

    yum -y install python-pip
    pip install pdfrw
    pip install Pillow==6.2.2
    amazon-linux-extras install python3.8 -y
    pip3.8 install PDFNetPython3==9.0.0
    sleep 30


    python3.8 /root/compile-spot-sqs/compress_pdf.py
    python /root/compile-spot-sqs/convert_pdf.py
    python /root/compile-spot-sqs/resize.py


    logger "Remove metadata from pdf files"

    sleep 30

    PATHFILE=$(find /tmp/latex/tmp/ -name '*.txt' -exec cat {} \;)

    # Try to run the latexmk command
    {
        latexmk -pdflatex=lualatex -interaction=batchmode -shell-escape -pdf -output-directory=/tmp/latex /tmp/latex/template.tex -f
        # pretend to do work for 60 seconds in order to catch the scale in protection
        logger "$0: Convert done. Copying to S3 and cleaning up"
        logger "$0: Running: aws s3 cp /tmp/latex/laudo_qualificacao.pdf s3://$S3BUCKETDESTINATION/$PATHFILE"
        cp /tmp/latex/template.pdf /tmp/latex/laudo_qualificacao.pdf
        FILE=/tmp/latex/laudo_qualificacao.pdf
        if [ -f "$FILE" ]; then
            aws s3 cp /tmp/latex/laudo_qualificacao.pdf s3://$S3BUCKETDESTINATION/$PATHFILE
            echo "echo 3 > /proc/sys/vm/drop_caches"
            logger "$0: Running: aws sqs --output=json delete-message --queue-url $SQSQUEUE --receipt-handle $RECEIPT"
            aws sqs --output=json delete-message --queue-url $SQSQUEUE --receipt-handle $RECEIPT
        else
          let COUNTER=COUNTER+1
          aws sns publish --topic-arn arn:aws:sns:us-east-1:013679769074:OttoProblem --message "Latexmk command failed to file: $PATHFILE"
          if [ $COUNTER -eq 3 ];then
              aws sqs --output=json delete-message --queue-url $SQSQUEUE --receipt-handle $RECEIPT
              let COUNTER=0
              rm -rf /tmp/latex/*
              continue
          else
            logger "Algo deu errado, bora contnuar: $COUNTER"
            rm -rf /tmp/latex/*
            continue
          fi
        fi
        sleep 60
        rm -rf /tmp/latex/*
        logger "$0: Running: aws autoscaling set-instance-protection --instance-ids $INSTANCE_ID --auto-scaling-group-name $AUTOSCALINGGROUP --no-protected-from-scale-in"
        aws autoscaling set-instance-protection --instance-ids $INSTANCE_ID --auto-scaling-group-name $AUTOSCALINGGROUP --no-protected-from-scale-in
    } || {
        # If the command fails, send an email using AWS SNS
        aws sns publish --topic-arn arn:aws:sns:us-east-1:013679769074:OttoProblem --message "Latexmk command failed to file: $PATHFILE"
    }
  else
    logger "$0: Skipping message - file not of type jpg, png, or gif. Deleting message from queue"

    aws sqs --output=json delete-message --queue-url $SQSQUEUE --receipt-handle $RECEIPT

  fi

done
